<?php
$menu = [
    ["id"=>1, "title" => "Thể thao", "parent_id"=>0],
    ["id"=>2, "title" => "Xã hội", "parent_id"=>0],
    ["id"=>3, "title" => "Thể thao trong nước", "parent_id"=>1],
    ["id"=>4, "title" => "Giao thông", "parent_id"=>2],
    ["id"=>5, "title" => "Môi trường", "parent_id"=>2],
    ["id"=>6, "title" => "Thể thao quốc tế", "parent_id"=>1],
    ["id"=>7, "title" => "Môi trường đô thị", "parent_id"=>5],
    ["id"=>8, "title" => "Giao thông ùn tắc", "parent_id"=>4]
];
function menu($data , $parent_id, $level = 0) {
    $result = [];
    foreach ($data as $item) {
        if($item['parent_id'] == $parent_id) {
            $item['level'] = $level;
            $result[] = $item;
            $child = menu($data, $item['id'], $level + 1);
            $result = array_merge($result, $child);
        }
    }
    return $result;
}
$result = menu($menu, 0);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<style>
    ul {
        list-style: none;
        text-decoration: none;
        background-color: #fff;
        width: 200px;
        border: 2px solid;
    }
    .list-item a {
        text-decoration: none;
    }
    .list-item:hover a{
        background-color: #2e45c6;
        color: #fff;
    }
</style>
<body>
<ul>
    <?php
    foreach ($result as $item) {
        ?>
        <li class = "list-item"><a href=""><?php echo str_repeat('---',$item['level']).$item['title'];?></a></li>
        <?php
    }
    ?>


</ul>
</body>
</html>