// Bài 9
class product{
    constructor(id, name, categoryId, saleDate, qulity, isDelete)
     {
    this.id = id;
    this.name = name;
    this.categoryId = categoryId;
    this.saleDate = saleDate;
    this.qulity = qulity;
    this.isDelete = isDelete;
}
}

// Bài 10
function listproduct(){
    const pushlistproduct = [];
    let product1 = new product(1, 'Chuột không dây', 1, new Date(), 0, true);
    pushlistproduct.push(product1);
    let product2 = new product(2, 'Chuột có dây', 1, new Date(), 2, true);
    pushlistproduct.push(product2);
    let product3 = new product(3, 'Bàn phím giả cơ', 1, new Date(), 0, true);
    pushlistproduct.push(product3);
    let product4 = new product(4, 'Bàn phím cơ', 3, new Date(), 1, false);
    pushlistproduct.push(product4);
    let product5 = new product(5, 'Bàn phím led', 1, new Date(), 2, false);
    pushlistproduct.push(product5);
    let product6 = new product(6, 'Lót chuột', 2, new Date(), 0, true);
    pushlistproduct.push(product6);
    let product7 = new product(7, 'Máy tính bàn', 1, new Date(), 3, false);
    pushlistproduct.push(product7);
    let product8 = new product(8, 'Máy tính laptop', 3, new Date(), 1, true);
    pushlistproduct.push(product8);
    let product9 = new product(9, 'Điện thoại Apple', 1, new Date(), 0, true);
    pushlistproduct.push(product9);
    let product10 = new product(10, 'Điện thoại Sam sung', 1, new Date(), 0, true);
    pushlistproduct.push(product10);
    return pushlistproduct;
}
const listProduct = listproduct();
console.table(listProduct);

// Bài 11
// ES6
function fiterProductById(listProduct, idProduct){
    const productname = listProduct.find(product => product.id === idProduct);
       return productname;
}
console.log(fiterProductById(listProduct,1));

// Dùng vòng For
function fiterProductByIdFor(listProduct, idProduct){
    for (let product of listProduct){
        if(product.id == idProduct){
            return product
        }
    }
    return false
}

const checkID = fiterProductByIdFor(listProduct, 10);
if (checkID != false){
    console.log(checkID);
}else {
    console.log('Không tồn tại');
}

// Bài 12
// ES6
function fiterProductByQuility(listProduct) {
    const fiterProduct = listProduct.filter(product => product.qulity > 0 && product.isDelete === false )
    return fiterProduct;
}

console.log(fiterProductByQuility(listProduct))

// Dùng for
function fiterProductByQulityFor(listProduct){
    const pushlistproduct = [];
    for (let product of listProduct){
        if(product.qulity > 0 && product.isdelete === false){
            pushlistproduct.push(product);
        }
    }
    if(!pushlistproduct.length){
        return false
    }
    return pushlistproduct
}
const checklistproductfor = fiterProductByQulityFor(listProduct);
if (checklistproductfor !== false){
    console.log(checklistproductfor);
}else {
    console.log('Không có giá trị nào hợp lệ');
}


// Bài 13
// ES6
function fiterProductBySaleDate(listProduct) {
    var newDate = new Date()
    const fiterProduct = listProduct.filter(product => product.saleDate - newDate.getDate() > 0 && product.isDelete === false )
    return fiterProduct
}

console.log(fiterProductBySaleDate(listProduct))
// Dùng for
function fiterProductBySaleDateFor(listProduct) {
    var newDate2 = new Date()
    const pushlistproduct2 = []
    for (let product of listProduct) {
        if(product.isDelete === false && product.saleDate  - newDate2.getDate() > 0) {
            pushlistproduct2.push(product);
        }
    }
    if(!pushlistproduct2.length) {
        return false
    }
    return pushlistproduct2
}
const checklistproductfor2 = fiterProductBySaleDateFor(listProduct);
if(checklistproductfor2 !== false) {
    console.log(checklistproductfor2)
}else {
    console.log('Rỗng')
}

// Bài 14
// Reduce
function filterProductReduce(listProduct) {
    const filterProduct3 = listProduct.filter(product => product.isDelete === false )
    const totalQuility = filterProduct3.reduce((total, num) => {
        return total + num.qulity
    },0);
    return totalQuility
}

console.log(filterProductReduce(listProduct))
// Không Reduce

function filterProductReduce2(listProduct) {
    var listproduct4 = []
    for(let product of listProduct) {
        if(product.isDelete === false) {
            listproduct4.push.qulity
        }
        if(!listproduct4.length) {
            return false
        }
        return listproduct4
    }
    const checklistproductfor4 = filterProductReduce2(listProduct);
    if(checklistproductfor4 != false) {
        console.log(checklistproductfor4)
    }else {
        console.log('Rỗng')
    }
}
// Bài 15
// ES6
function isHaveProductInCategory(listProduct,categoryId) {
    const categoryid = listProduct.filter(product => product.categoryId === categoryId)
    if(categoryid) {
        return true
    }
    return false
}

console.log(isHaveProductInCategory(listProduct, 3))
// Dùng for
function isHaveProductInCategory2(listProduct,categoryid) {
    for (let product of listProduct ) {
        if (product.categoryId === categoryid){
            return true
        }
      
    }
    return false
}

console.log(isHaveProductInCategory2(listProduct, 3))
// Bài 16
// ES6
function fiterProductSaleDateQulity(listProduct) {
    var nowDate = new Date()
    const filterProduct2 = listProduct.filter(product => product.saleDate - nowDate.getDate() > 0 && product.qulity > 0  )
    const listreduceproduct = filterProduct2.map((item) => {
        return {id: item.id, name:item.name}
    });
    return listreduceproduct
}

console.log(fiterProductSaleDateQulity(listProduct))
// Dùng for
function fiterProductSaleDateQulity2(listProduct) {
    var listProduct3 = []
    var nowDate2 = new Date()
    for(let product of listProduct) {
        if(product.saleDate - nowDate2.getDate() > 0 && product.qulity > 0) {
            listProduct3.push(product.id, product.name)
        }
    }
    if(!listProduct3.length){
        return false
    }
    return listProduct3
}   
const checklistproductfor3 = fiterProductSaleDateQulity2(listProduct);
if(checklistproductfor3 !== false) {
    console.log(checklistproductfor3)
}else {
    console.log('Rỗng')
}


