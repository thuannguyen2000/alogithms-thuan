<?php
require_once ('./../dao/Database.php');
require_once ('./../entity/Product.php');
require_once ('./../entity/Category.php');
require_once ('./../entity/Accessory.php');
require_once('./../interface/IDao.php');

abstract class BaseDao implements IDao {

    protected $database;

    public function __construct() 
    {
        $this ->database = Database::getInstants();
    }

     /*
     * Get name table from param row
     * @param $row  
     * @return string
     */
    public function getTableName($row)
    {
        return strtolower(get_class($row)).'Table';
    }

     /*
     * Insert row 
     * @param $row
     * @return void
     */
    public function insert($row) {
        $this -> database->insertTable($this->getTableName(), $row);
    }

     /*
     * Update row 
     * @param $row
     * @return void
     */
    public function update( $row) {
        $this -> database->updateTable($this->getTableName(), $row);
    }

     /*
     * Delete row to 
     * @param $row
     * @return void
     */
    public function delelte( $row) {
        $this -> database->deleteTable($this->getTableName(), $row);
    }

     /*
     * SelectAll row 
     * @param $row
     * @return void
     */
    public function findAll() {
        return  $this -> database->selectTable($name);
    }
    
     /*
     * Select row to By ID
     * @param $row
     * @return void
     */
    public function findById($name, $id) {
       return $this-> database->selectTable($name, $id);
    }

    public function initDatabase()
    {
        for($i = 1; $i<=10 ; $i++)
        {
            $product = new Product($i, 'Chuột không dây'.$i, 2);
            $this->insert($product);

            $category = new Category($i, 'Phụ kiện máy tính'.$i);
            $this->insert($category);

            $accessotion = new Accessotion($i, 'Accessotion '.$i);
            $this->insert($accessotion);
        }
    }
}

/*
Lợi ích của abstract là
có thể khai báo các function giống nhau
có thể rút gọn code lại 
dễ dàng sửa code khi bị sai
*/