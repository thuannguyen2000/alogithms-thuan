<?php
require_once ('./../interface/IEntity.php');

 abstract class BaseRow implements IEntity {

    protected $id;
    protected $name;

    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

     /*
     * Get ID by Row
     * @return id
     *
    */
    public function getId()
    {
        return $this->id;
    }

    /*
     * Set Id for Row
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

     /*
     * Get Name by Row
     * @return id
     *
    */
    public function getName()
    {
        return $this->name;
    }

     /*
     * Set Name for Row
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

}
/* Lợi ích của abstract là
có thể khai báo các function giống nhau
có thể rút gọn code lại 
dễ dàng sửa code khi bị sai
*/