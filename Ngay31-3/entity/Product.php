<?php
require_once ('./../abstract/BaseRow.php');

    class Product extends BaseRow {
        private $categoryId;

        public function __construct($categoryId)
        {
            $this->categoryId = $categoryId;
        }
        
        /*
        * Get CategoryID  by row
        * @param $row
        * @return void
        */
        public function getCategoryId()
        {
            return $this->categoryId;
        }

        /*
        * Set CategoryID  by row
        * @param $row
        * @return void
        */
        public function setCategoryId($categoryId)
        {
            $this->categoryId = $categoryId;
        }
    }
?>