<?php

interface IEntity
{
    /*
     * get ID by instant
     * @return string
     */
    public function getId();

    /*
     * get Name by instant
     * @return string
     */
    public function getName();
}
