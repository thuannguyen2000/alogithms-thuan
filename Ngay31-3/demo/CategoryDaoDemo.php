<?php
require_once ('./../dao/Database.php');
require_once ('./../entity/Category.php');

class CategoryDaoDemo extends Database {
      
    /*
     * insert  Category
     * @param  $id
     * @param  $name
     * return mixed
     */
    function insertTest(Category $row) {
        $this -> insertTable('categoryTable', $row); 
    }

     /*
     * findAll  Category
     * @param  $id
     * @param  $name
     * return mixed
     */
    function findAllTest(Category $row) {
        return $this->selectTable('categoryTable');
    }

     /*
     * update  Category
     * @param  $id
     * @param  $name
     * return mixed
     */
    function updateTest(Category $row) {
        $this -> updateTable('categoryTable', $row); 
    }

     /*
     * delete  Category
     * @param  $id
     * @param  $name
     * return mixed
     */
    function deleteTest(Category $row) {
        $this -> deleteTable('categoryTable', $row);
    }
}