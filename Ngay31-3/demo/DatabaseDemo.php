<?php
require_once ('./../dao/Database.php');
require_once ('./../entity/Category.php');
require_once ('./../entity/Product.php');
require_once ('./../entity/Accessory.php');

class DatabaseDemo extends  Database {
    /*
     * Insert row to Table
     * @param  $name
     * @param  $row
     * @return void
     */
    public function insertTableTest($name, $row) {
        $this -> insertTable();
    }

    /*
     * Select all row from Table
     * @param  $name
     * @return mixed
     */
    public function selectTableTest($name, $whereId = null) {
       return $this -> selectTable();
    }

    /*
     * Update row from Table by ID
     * @param  $name
     * @param  $row
     * @return void
     */
    public function updateTableTest($name, $row) {
        $this -> updateTable();
    }

    /*
     * Delete row from Table by ID
     * @param  $name
     * @param  $row
     * @return void
     */
    public function deleteTableTest($name, $row) {
        $this -> deleteTable();
    }

    /*
     * Delete all row from Table
     * @param  $name
     * @return void
     */
    public function truncateTable($name) {
        $this -> truncateTable();
    }

    /*
     * Init row to Table
     * @param  $name
     * @param  $row
     * @return void
     */
    public function initDatabase()
    {
        for ($i = 0; $i <11; $i++)
        {
            $productNew = new product($i, 'Chuột không dây'.$i, 2);
            $this->insertTable('productTable',$productNew);

            $categoryNew = new category($i, 'Danh mục'.$i);
            $this->insertTable('categoryTable', $categoryNew);

            $accessoryNew = new accessory($i, 'Accessotion '.$i);
            $this->insertTable('accessoryTable', $accessoryNew);
        }
    }
    public function  printDatabase($database){
    $database = new DatabaseDemo();
    $database->initDatabase();
    echo '<pre>';
    print_r(($database->selectTableTest('categoryTable')));
    }
}
